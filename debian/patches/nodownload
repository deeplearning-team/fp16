Index: fp16/CMakeLists.txt
===================================================================
--- fp16.orig/CMakeLists.txt
+++ fp16/CMakeLists.txt
@@ -34,13 +34,11 @@ SET(CONFU_DEPENDENCIES_BINARY_DIR ${CMAK
   CACHE PATH "Confu-style dependencies binary directory")
 
 IF(NOT DEFINED PSIMD_SOURCE_DIR)
-  MESSAGE(STATUS "Downloading PSimd to ${CONFU_DEPENDENCIES_SOURCE_DIR}/psimd (define PSIMD_SOURCE_DIR to avoid it)")
-  CONFIGURE_FILE(cmake/DownloadPSimd.cmake "${CONFU_DEPENDENCIES_BINARY_DIR}/psimd-download/CMakeLists.txt")
-  EXECUTE_PROCESS(COMMAND "${CMAKE_COMMAND}" -G "${CMAKE_GENERATOR}" .
-    WORKING_DIRECTORY "${CONFU_DEPENDENCIES_BINARY_DIR}/psimd-download")
-  EXECUTE_PROCESS(COMMAND "${CMAKE_COMMAND}" --build .
-    WORKING_DIRECTORY "${CONFU_DEPENDENCIES_BINARY_DIR}/psimd-download")
-  SET(PSIMD_SOURCE_DIR "${CONFU_DEPENDENCIES_SOURCE_DIR}/psimd" CACHE STRING "PSimd source directory")
+	find_file(PSIMD_HDR psimd.h PATH_SUFFIXES include)
+	if(NOT PSIMD_HDR)
+		message(FATAL_ERROR "Cannot find PSIMD")
+	endif()
+	message("-- Found psimd: ${PSIMD_HDR}")
 ENDIF()
 
 IF(FP16_BUILD_TESTS AND NOT DEFINED GOOGLETEST_SOURCE_DIR)
@@ -54,13 +52,10 @@ IF(FP16_BUILD_TESTS AND NOT DEFINED GOOG
 ENDIF()
 
 IF(FP16_BUILD_BENCHMARKS AND NOT DEFINED GOOGLEBENCHMARK_SOURCE_DIR)
-  MESSAGE(STATUS "Downloading Google Benchmark to ${CONFU_DEPENDENCIES_SOURCE_DIR}/googlebenchmark (define GOOGLEBENCHMARK_SOURCE_DIR to avoid it)")
-  CONFIGURE_FILE(cmake/DownloadGoogleBenchmark.cmake "${CONFU_DEPENDENCIES_BINARY_DIR}/googlebenchmark-download/CMakeLists.txt")
-  EXECUTE_PROCESS(COMMAND "${CMAKE_COMMAND}" -G "${CMAKE_GENERATOR}" .
-    WORKING_DIRECTORY "${CONFU_DEPENDENCIES_BINARY_DIR}/googlebenchmark-download")
-  EXECUTE_PROCESS(COMMAND "${CMAKE_COMMAND}" --build .
-    WORKING_DIRECTORY "${CONFU_DEPENDENCIES_BINARY_DIR}/googlebenchmark-download")
-  SET(GOOGLEBENCHMARK_SOURCE_DIR "${CONFU_DEPENDENCIES_SOURCE_DIR}/googlebenchmark" CACHE STRING "Google Benchmark source directory")
+	add_library(benchmark SHARED IMPORTED)
+	find_library(BENCHMARK_LIBRARY benchmark)
+	message("-- Found benchmark: ${BENCHMARK_LIBRARY}")
+	set_target_properties(benchmark PROPERTIES IMPORTED_LOCATION "${BENCHMARK_LIBRARY}")
 ENDIF()
 
 # ---[ FP16 library
@@ -89,13 +84,6 @@ INSTALL(FILES
     include/fp16/avx2.py
   DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/fp16)
 
-# ---[ Configure psimd
-IF(NOT TARGET psimd)
-  ADD_SUBDIRECTORY(
-    "${PSIMD_SOURCE_DIR}"
-    "${CONFU_DEPENDENCIES_BINARY_DIR}/psimd")
-ENDIF()
-
 IF(FP16_BUILD_TESTS)
   # ---[ Build google test
   IF(NOT TARGET gtest)
@@ -154,28 +142,28 @@ IF(FP16_BUILD_BENCHMARKS)
   ADD_EXECUTABLE(ieee-element-bench bench/ieee-element.cc)
   TARGET_COMPILE_DEFINITIONS(ieee-element-bench PRIVATE FP16_COMPARATIVE_BENCHMARKS=1)
   TARGET_INCLUDE_DIRECTORIES(ieee-element-bench PRIVATE ${PROJECT_SOURCE_DIR})
-  TARGET_LINK_LIBRARIES(ieee-element-bench fp16 psimd benchmark)
+  TARGET_LINK_LIBRARIES(ieee-element-bench fp16 benchmark pthread)
 
   ADD_EXECUTABLE(alt-element-bench bench/alt-element.cc)
-  TARGET_LINK_LIBRARIES(alt-element-bench fp16 psimd benchmark)
+  TARGET_LINK_LIBRARIES(alt-element-bench fp16 benchmark pthread)
 
   ADD_EXECUTABLE(from-ieee-array-bench bench/from-ieee-array.cc)
   FP16_TARGET_ENABLE_CXX11(from-ieee-array-bench)
   TARGET_COMPILE_DEFINITIONS(from-ieee-array-bench PRIVATE FP16_COMPARATIVE_BENCHMARKS=1)
   TARGET_INCLUDE_DIRECTORIES(from-ieee-array-bench PRIVATE ${PROJECT_SOURCE_DIR})
-  TARGET_LINK_LIBRARIES(from-ieee-array-bench fp16 psimd benchmark)
+  TARGET_LINK_LIBRARIES(from-ieee-array-bench fp16 benchmark pthread)
 
   ADD_EXECUTABLE(from-alt-array-bench bench/from-alt-array.cc)
   FP16_TARGET_ENABLE_CXX11(from-alt-array-bench)
-  TARGET_LINK_LIBRARIES(from-alt-array-bench fp16 psimd benchmark)
+  TARGET_LINK_LIBRARIES(from-alt-array-bench fp16 benchmark pthread)
 
   ADD_EXECUTABLE(to-ieee-array-bench bench/to-ieee-array.cc)
   FP16_TARGET_ENABLE_CXX11(to-ieee-array-bench)
   TARGET_COMPILE_DEFINITIONS(to-ieee-array-bench PRIVATE FP16_COMPARATIVE_BENCHMARKS=1)
   TARGET_INCLUDE_DIRECTORIES(to-ieee-array-bench PRIVATE ${PROJECT_SOURCE_DIR})
-  TARGET_LINK_LIBRARIES(to-ieee-array-bench fp16 psimd benchmark)
+  TARGET_LINK_LIBRARIES(to-ieee-array-bench fp16 benchmark pthread)
 
   ADD_EXECUTABLE(to-alt-array-bench bench/to-alt-array.cc)
   FP16_TARGET_ENABLE_CXX11(to-alt-array-bench)
-  TARGET_LINK_LIBRARIES(to-alt-array-bench fp16 psimd benchmark)
+  TARGET_LINK_LIBRARIES(to-alt-array-bench fp16 benchmark pthread)
 ENDIF()
